import "./App.css";
import formData from "./misc/formData.json";
import DynamicFormBuilder from "./components/DynamicFormBuilder";
import React from "react";

function App() {
  return (
    <div className="container">
      <DynamicFormBuilder
        formElementsDefs={formData}
        onSubmit={(data) => console.log(data)}
      />
    </div>
  );
}

export default App;
