import React, {useCallback, useMemo, useState} from "react";
import TextFormElement from "./formElements/TextFormElement";
import CheckBoxFormElement from "./formElements/CheckBoxFormElement";
import SelectFormElement from "./formElements/SelectFormElement";

import {processFormElementValidity, processFormValidity} from './formValidation/FormValidator';
 
export default function DynamicFormBuilder({formElementsDefs, onSubmit}) {
    const [isFormValidated, setIsFormValidated] = useState(false);
    const [formDataState, setFromDataState] = useState(
        () => prepInitFormDataState(formElementsDefs)
    );
    const [formValidityTxtState, setFormValidityTxtState] = useState(
        () => prepInitValidityTxtState(formElementsDefs)
    );

    const validityMessages = useMemo(() => {
        return formElementsDefs.reduce((prev, current) => {
            return {...prev, [current.id]: current.validityStateMsg}
        }, {});
    }, [formElementsDefs]);

    const onChange = useCallback(
            (value, id, target) => {
        setFromDataState((prev) => {
            return {...prev, [id]: value};
        });

        setFormValidityTxtState(prev => {
            return {...prev, [id]: processFormElementValidity(target, validityMessages[id]).invalidityText}
        })
    }, [validityMessages]);

    const onSubmitWrapper = useCallback((event) => {
        const HTMLForm = event.target;
        const formValidity = processFormValidity(HTMLForm, validityMessages);
        if (formValidity.isValid) {
            onSubmit(formDataState);
        }
        setIsFormValidated(true);
        setFormValidityTxtState(formValidity.invalidityMessages);
        event.preventDefault();
    }, [onSubmit, formDataState, validityMessages]);

    return (
        <form noValidate onSubmit={onSubmitWrapper} className={isFormValidated ? "was-validated" : ""}>
            {formElementsDefs
                .filter(elDef => isElementVisible(elDef, formDataState))
                .map((elDef) => renderElementBasedOnType(
                    elDef,
                    formDataState[elDef.id],
                    formValidityTxtState[elDef.id],
                    onChange
                ))}
            <input type="submit" name="submit" className="btn btn-primary" value="Submit"/>
        </form>
    );
}

function prepInitFormDataState(formElementsDefs) {
    return formElementsDefs.reduce((state, elDef) => {
        return {...state, [elDef.id]: elDef.value === undefined ? "" : elDef.value};
    }, {});
}

function prepInitValidityTxtState(formElementsDefs) {
    return formElementsDefs.reduce((state, elDef) => {
        return {...state, [elDef.id]: ""};
    }, {});
}

function renderElementBasedOnType(elDef, value, invalidityTxt, onChange) {
    const formElementProps = {
        ...elDef,
        key: elDef.id,
        value: value,
        onChange,
        invalidityText: invalidityTxt,
    };

    return {
        'text': () => <TextFormElement {...formElementProps} />,
        'email': () => <TextFormElement {...formElementProps} />,
        'checkbox': () => <CheckBoxFormElement {...formElementProps} />,
        'select': () => <SelectFormElement {...formElementProps} />
    }[elDef.type]();
}

function isElementVisible(elDef, formDataState) {
    if (!elDef.visibility) {
        return true;
    }

    const {dependentProperty, expectedValue, negation} = elDef.visibility;
    return Boolean(
        negation ^
        Number(formDataState[dependentProperty] === expectedValue)
    );
}
