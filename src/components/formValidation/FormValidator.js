export function processFormElementValidity(element, customValidationMessages) {
    const {validity, validationMessage} = element;
    if (validity.isValid) {
        return {
            isValid: false,
            invalidityText: null,
        };
    }

    let invalidityText = validationMessage;
    if (customValidationMessages) {
        for (let key in validity) {
            if (key !== "valid" && validity[key]) {
                invalidityText = customValidationMessages[key] || invalidityText;
                break;
            }
        }
    }

    return {
        isValid: true,
        invalidityText,
    };
}

export function processFormValidity(form, customValidationMessages) {
    return [...form.elements]
        .filter((f) => f.type !== "submit")
        .reduce(
            (validityResult, formField) => {
                const {isValid, invalidityText} = processFormElementValidity(formField, customValidationMessages[formField.name]);
                validityResult.isValid = validityResult.isValid && isValid;
                validityResult.invalidityMessages[formField.name] = invalidityText;
                return validityResult;
            },
            {
                isValid: true,
                invalidityMessages: {}
            }
        );
}
