import React, { memo, useCallback } from "react";
import FormElementWrapper from "../formElementWrapper/FormElementWrapper";

function TextFormElement(props) {
  const {
    id,
    label,
    type,
    placeholder,
    value,
    validity,
    invalidityText,
    onChange,
  } = props;

  const onChangeHandler = useCallback(
    ({ target }) => {
      onChange(target.value, target.id, target);
    },
    [onChange]
  );

  return (
    <FormElementWrapper
      formInputId={id}
      labelText={label}
      isRequired={validity?.required}
      invalidityFeedback={invalidityText}
    >
      <input
        id={id}
        name={id}
        type={type}
        placeholder={placeholder}
        value={value}
        className="form-control"
        onChange={onChangeHandler}
        required={validity?.required}
        maxLength={validity?.maxLength}
        minLength={validity?.minLength}
        pattern={validity?.pattern}
      />
    </FormElementWrapper>
  );
}

export default memo(TextFormElement);
