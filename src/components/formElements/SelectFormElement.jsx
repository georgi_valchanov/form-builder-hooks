import React, {memo, useCallback, useMemo} from "react";
import FormElementWrapper from "../formElementWrapper/FormElementWrapper";

export default memo(function SelectFormElement(props) {

    const {
        id,
        label,
        validity,
        invalidityText,
        onChange
        
    } = props;

    const options = useMemo(() => ([
        {value: "", label: "--please select a value--", isDisabled: true},
        ...props.options,
    ]), [props.options]);

    const onChangeWrapper = useCallback(({target}) => {
        onChange(target.value, target.id, target);
    }, [onChange]);

    return (
        <FormElementWrapper
            formInputId={id}
            labelText={label}
            isRequired={validity?.required}
            invalidityFeedback={invalidityText}
        >
            <select
                id={id}
                name={id}
                defaultValue={""}
                required={validity?.required}
                onChange={onChangeWrapper}
                className="form-control"
            >
                {options.map((o) => (
                    <option key={o.value} value={o.value} disabled={o.isDisabled}>
                        {o.label}
                    </option>
                ))}
            </select>
        </FormElementWrapper>
    );
});
