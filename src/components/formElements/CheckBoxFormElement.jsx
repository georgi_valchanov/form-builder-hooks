import React, { memo, useCallback } from "react";
import FormElementWrapper from "../formElementWrapper/FormElementWrapper";

function CheckBoxFormElement(props) {
  const {
    id,
    label,
    validity,
    invalidityText,
    onChange,
    value = false,
  } = props;

  const onChangeHandler = useCallback(
    ({ target }) => {
      onChange(target.checked, target.id, target);
    },
    [onChange]
  );

  return (
    <FormElementWrapper
      formInputId={id}
      labelText={label}
      isRequired={validity?.required}
      invalidityFeedback={invalidityText}
      inputIsCheckbox={true}
    >
      <input
        type="checkbox"
        id={id}
        name={id}
        required={validity?.required}
        value={value}
        className="form-check-input"
        onChange={onChangeHandler}
      />
    </FormElementWrapper>
  );
}

export default memo(CheckBoxFormElement);
