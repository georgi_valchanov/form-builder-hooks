import React, { memo, useMemo } from "react";

function FormLabel(props) {
  const { forId, text, required = false, isCheckBoxLabel = false } = props;

  const cssClasses = useMemo(() => {
    const cssClasses = ["form-label"];
    if (isCheckBoxLabel) {
      cssClasses.push("form-check-label");
    }

    return cssClasses.join(" ");
  }, [isCheckBoxLabel]);

  return (
    <label htmlFor={forId} className={cssClasses}>
      <span>{text}</span>
      {required && <span style={{ color: "red" }}>*</span>}
    </label>
  );
}
export default memo(FormLabel);
