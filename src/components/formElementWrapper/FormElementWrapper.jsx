import React, { memo } from "react";
import FormLabel from "./FormLabel";

function FormElementWrapper(props) {
  const {
    formInputId,
    inputIsCheckbox = false,
    labelText,
    isRequired = false,
    invalidityFeedback,
    children,
  } = props;

  return (
    <div className={inputIsCheckbox ? "mb-3 form-check" : "mb-3"}>
      {Boolean(inputIsCheckbox) && children}
      <FormLabel
        forId={formInputId}
        text={labelText}
        required={isRequired}
        isCheckBoxLabel={inputIsCheckbox}
      />
      {!inputIsCheckbox && children}
      {invalidityFeedback && (
        <div className="invalid-feedback">{invalidityFeedback}</div>
      )}
    </div>
  );
}

export default memo(FormElementWrapper);